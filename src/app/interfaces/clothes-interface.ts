import {KeyValueInterface} from './key-value-interface';

export interface ClothesInterface {
    id?: number;
    size: number;
    color: string;
    type: string;
    price: number;
    description: string;
}
