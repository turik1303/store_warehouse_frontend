import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LocationComponentComponent} from './locations/location-component/location-component.component';
import {DashboardComponent} from './locations/dashboard/dashboard.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { WarehouseComponent } from './locations/warehouse/warehouse.component';

@NgModule({
    declarations: [
        AppComponent,
        LocationComponentComponent,
        DashboardComponent,
        WarehouseComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [HttpClientModule],
    bootstrap: [AppComponent]
})
export class AppModule {
}
