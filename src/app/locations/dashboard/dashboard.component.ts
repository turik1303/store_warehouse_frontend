import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  clothesUpdated: Subject<boolean>;

  constructor() {
    this.clothesUpdated = new Subject();
  }

  ngOnInit() {
  }

}
