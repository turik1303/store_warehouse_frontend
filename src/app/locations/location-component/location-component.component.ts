import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ConstantsService} from '../../services/constants.service';
import {ClothesInterface} from '../../interfaces/clothes-interface';
import {KeyValueInterface} from '../../interfaces/key-value-interface';
import {map, switchMap} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-location-component',
    templateUrl: './location-component.component.html',
    styleUrls: ['./location-component.component.scss']
})
export class LocationComponentComponent implements OnInit {
    @Input('update') update: Subject<boolean>;
    updateSubscription: Subscription;
    storeClothes: ClothesInterface[];
    newClothes: ClothesInterface;
    sizes: number[];
    colors: KeyValueInterface[];
    types: KeyValueInterface[];


    editClothFormInStore: FormGroup;

    constructor(
        private apiService: ApiService,
        private constants: ConstantsService
    ) {
        this.newClothes = {
            size: null,
            color: null,
            type: null,
            price: null,
            description: ''
        };

        this.sizes = constants.getSizes();
        this.colors = constants.getColors();
        this.types = constants.getTypes();


        this.editClothFormInStore = new FormGroup({
            type: new FormControl(),
            size: new FormControl(),
            color: new FormControl(),
            price: new FormControl(),
            description: new FormControl()
        });
    }

    ngOnInit() {
        this.storeClothes = [];
        this.updateSubscription = this.update.pipe(
            switchMap((next) => {
                return this.apiService.getClothesByLocation('store');
            }),
            map((res) => {
                this.changeNames(res);
                this.storeClothes = res;
            }))
            .subscribe();

        this.apiService.getClothesByLocation('store')
            .subscribe(
                (res) => {
                    this.changeNames(res);
                    this.storeClothes = res;
                },
                (err) => {
                    alert(err.message);
                }
            );
    }

    private changeNames(res) {
        res.forEach(item => {
            item.color = this.colors.find(x => x.key === item.color).value;
            item.type = this.types.find(x => x.key === item.type).value;
        });
    }

    removeClothes(id: number) {
        this.apiService.delete(id)
            .subscribe(
                () => {
                    this.update.next(true);
                },
                (err) => {
                    alert(err.message);
                }
            );
    }

    move(id: number) {
        this.apiService.move('store', id)
            .subscribe(
                () => {
                    this.update.next(true);
                },
                (err) => {
                    alert(err.message);
                });
    }

    startEditCloth(cloth) {
        this.editClothFormInStore = new FormGroup({
            type: new FormControl(this.types.find(x => x.value === cloth.type).key, [Validators.required]),
            size: new FormControl(cloth.size, [Validators.required]),
            color: new FormControl(this.colors.find(x => x.value === cloth.color).key, [Validators.required]),
            price: new FormControl(cloth.price, [Validators.required, Validators.pattern('[0-9.,]*')]),
            description: new FormControl(cloth.description, [Validators.required])
        });
    }

    updateCloth(cloth) {
        if (this.editClothFormInStore.valid) {
            cloth.color = this.editClothFormInStore.get('color').value;
            cloth.size = this.editClothFormInStore.get('size').value;
            cloth.type = this.editClothFormInStore.get('type').value;
            cloth.price = this.editClothFormInStore.get('price').value;
            cloth.description = this.editClothFormInStore.get('description').value;

            this.apiService.update(cloth)
                .subscribe(
                    () => {
                        this.update.next(true);
                    },
                    (err) => {
                        alert(err.message);
                    });
        }
    }
}
