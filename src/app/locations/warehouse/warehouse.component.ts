import {Component, Input, OnInit} from '@angular/core';
import {ClothesInterface} from '../../interfaces/clothes-interface';
import {KeyValueInterface} from '../../interfaces/key-value-interface';
import {ApiService} from '../../services/api.service';
import {ConstantsService} from '../../services/constants.service';
import {map, switchMap} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject, Subscribable, Subscription} from 'rxjs';
import {el} from '@angular/platform-browser/testing/src/browser_util';

@Component({
    selector: 'app-warehouse',
    templateUrl: './warehouse.component.html',
    styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
    @Input('update') update: Subject<boolean>;
    updateSubscription: Subscription;


    warehouseClothes: ClothesInterface[];
    newClothes: ClothesInterface;
    sizes: number[];
    colors: KeyValueInterface[];
    types: KeyValueInterface[];

    newClothForm: FormGroup;
    editClothFormInWareHouse: FormGroup;


    constructor(
        private apiService: ApiService,
        private constants: ConstantsService
    ) {
        this.newClothes = {
            size: null,
            color: null,
            type: null,
            price: null,
            description: ''
        };

        this.sizes = constants.getSizes();
        this.colors = constants.getColors();
        this.types = constants.getTypes();


        this.newClothForm = new FormGroup({
            type: new FormControl(null, [Validators.required]),
            size: new FormControl(null, [Validators.required]),
            color: new FormControl(null, [Validators.required]),
            price: new FormControl(null, [Validators.required, Validators.pattern('[0-9.,]*')]),
            description: new FormControl(null, [Validators.required])
        });

        this.editClothFormInWareHouse = new FormGroup({
            type: new FormControl(),
            size: new FormControl(),
            color: new FormControl(),
            price: new FormControl(),
            description: new FormControl()
        });
    }

    ngOnInit() {
        this.warehouseClothes = [];
        this.updateSubscription = this.update.pipe(
            switchMap((next) => {
                return this.apiService.getClothesByLocation('warehouse');
            }),
            map((res) => {
                this.changeNames(res);
                this.warehouseClothes = res;
            }))
            .subscribe();

        this.apiService.getClothesByLocation('warehouse')
            .subscribe(
                (res) => {
                    this.changeNames(res);
                    this.warehouseClothes = res;
                },
                (err) => {
                    alert(err.message);
                }
            );
    }

    private changeNames(res) {
        res.forEach(item => {
            item.color = this.colors.find(x => x.key === item.color).value;
            item.type = this.types.find(x => x.key === item.type).value;
        });
    }

    createNewClothes() {
        if (this.newClothForm.valid) {
            this.newClothes.color = this.newClothForm.get('color').value;
            this.newClothes.type = this.newClothForm.get('type').value;
            this.newClothes.size = this.newClothForm.get('size').value;
            this.newClothes.price = this.newClothForm.get('price').value;
            this.newClothes.description = this.newClothForm.get('description').value;

            this.apiService.create(this.newClothes)
                .subscribe(
                    () => {
                        this.newClothForm.reset();
                        this.update.next(true);
                    },
                    (err) => {
                        alert(err.message);
                    }
                );
        }
    }

    removeClothes(id: number) {
        this.apiService.delete(id)
            .subscribe(
                () => {
                    this.update.next(true);
                },
                (err) => {
                    alert(err.message);
                }
            );
    }

    move(id: number) {
        this.apiService.move('warehouse', id)
            .subscribe(
                () => {
                    this.update.next(true);
                },
                (err) => {
                    alert(err.message);
                }
            );
    }

    startEditCloth(cloth: ClothesInterface) {
        this.editClothFormInWareHouse = new FormGroup({
            type: new FormControl(this.types.find(x => x.value === cloth.type).key, [Validators.required]),
            size: new FormControl(cloth.size, [Validators.required]),
            color: new FormControl(this.colors.find(x => x.value === cloth.color).key, [Validators.required]),
            price: new FormControl(cloth.price, [Validators.required, Validators.pattern('[0-9.,]*')]),
            description: new FormControl(cloth.description, [Validators.required])
        });
    }

    updateCloth(cloth: ClothesInterface) {
        if (this.editClothFormInWareHouse.valid) {
            cloth.color = this.editClothFormInWareHouse.get('color').value;
            cloth.size = this.editClothFormInWareHouse.get('size').value;
            cloth.type = this.editClothFormInWareHouse.get('type').value;
            cloth.price = this.editClothFormInWareHouse.get('price').value;
            cloth.description = this.editClothFormInWareHouse.get('description').value;

            this.apiService.update(cloth)
                .subscribe(
                    () => {
                        this.update.next(true);
                        this.editClothFormInWareHouse = new FormGroup({
                            type: new FormControl(),
                            size: new FormControl(),
                            color: new FormControl(),
                            price: new FormControl(),
                            description: new FormControl()
                        });
                    },
                    (err) => {
                        alert(err.message);
                    });
        }
    }


}
