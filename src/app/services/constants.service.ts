import {Injectable} from '@angular/core';
import {KeyValueInterface} from '../interfaces/key-value-interface';

@Injectable({
    providedIn: 'root'
})
export class ConstantsService {
    private sizes: number[];
    private colors: KeyValueInterface[];
    private types: KeyValueInterface[];

    constructor() {
        this.sizes = [42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54];
        this.colors = [
            {key: 'WHITE', value: 'Белый'},
            {key: 'BLUE', value: 'Синий'},
            {key: 'RED', value: 'Красный'},
            {key: 'GREEN', value: 'Зеленый'},
            {key: 'BLACK', value: 'Черный'}
        ];
        this.types = [
            {key: 'DRESS', value: 'Платье'},
            {key: 'TROUSERS', value: 'Брюки'},
            {key: 'SKIRT', value: 'Юбка'},
            {key: 'VEST', value: 'Жилет'},
            {key: 'SHIRT', value: 'Рубашка'},
        ];
    }

    public getSizes(): number[] {
        return this.sizes;
    }

    public getColors(): KeyValueInterface[] {
        return this.colors;
    }

    public getTypes(): KeyValueInterface[] {
        return this.types;
    }

}
