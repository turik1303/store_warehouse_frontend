import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ClothesInterface} from '../interfaces/clothes-interface';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private apiUrl: string;

    constructor(private http: HttpClient) {
        this.apiUrl = 'http://localhost:8080/store-0.0.1-SNAPSHOT';
    }

    public getAllClothes(): Observable<ClothesInterface[]> {
        return this.http.get<ClothesInterface[]>(`${this.apiUrl}/clothes/`);
    }

    public getClothesByLocation(location: string): Observable<ClothesInterface[]> {
        return this.http.get<ClothesInterface[]>(`${this.apiUrl}/clothes/by-location/${location}`);
    }

    public getClothesById(id: number): Observable<ClothesInterface> {
        return this.http.get<ClothesInterface>(`${this.apiUrl}/clothes/${id}`);
    }

    public create(clothes: ClothesInterface): Observable<ClothesInterface> {
        return this.http.put<ClothesInterface>(`${this.apiUrl}/clothes/`, clothes);
    }

    public update(clothes: ClothesInterface): Observable<void> {
        return this.http.patch<void>(`${this.apiUrl}/clothes/`, clothes);
    }

    public delete(id: number): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/clothes/${id}`);
    }

    public move(from: string, id: number): Observable<void> {
        const req = {from, id};
        return this.http.post<void>(`${this.apiUrl}/clothes/move`, req);
    }
}
